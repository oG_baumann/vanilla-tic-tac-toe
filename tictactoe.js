!function(){

	var _this = null;

	function app () {
		return new app.fn.init();
	}

	app.fn = app.prototype = {
		app		: document.getElementById('app'),
		box		: '<div class="box"><div class="inner"><div></div><div></div></div></div>',
		menu	: '<div class="menu"><div class="play">Play</div></div>', 
		virtual	: [[],[],[]],
		board 	: [[0,0,0],[0,0,0],[0,0,0]],
		player  : ['x',-1],
		comp 	: ['o',1],
		turn    : null,
		step	: 0
	};

	app.fn.init = function () {
		_this = this;
		_this.turn = _this.player;
		var i = 0;
		var x = 0, y = 0;
		var tmp = document.createElement('DIV');
		tmp.innerHTML = _this.menu;
		_this.menu = tmp.firstElementChild;
		_this.menu.addEventListener('click', _this.reset);
		_this.app.appendChild(_this.menu);
		while(i < 9){
			tmp = document.createElement('DIV');
			tmp.innerHTML = _this.box;
			tmp = tmp.firstElementChild;
			tmp.addEventListener('click', _this.move);
			tmp.coords = [x,y];
			_this.virtual[x][y] = tmp;
			_this.app.appendChild(tmp);
			y++;
			if(y > 2) {
				y = 0;
				x++;
			}
			i++;
		}
		window['og_loader'] = 'loaded';
	}

	app.fn.reset = function () {
		var x = 0, y = 0;
		while(x < _this.virtual.length) {
			_this.virtual[x][y].classList = "box";
			y++;
			if(y > 2) {
				y = 0;
				x++;
			}
		}
		_this.turn = _this.player;
		_this.step = 0;
		_this.board = [[0,0,0],[0,0,0],[0,0,0]];
		this.style.display = 'none';
		this.classList = 'menu';
	}

	app.fn.move = function(e) {
		if(_this.turn === _this.comp && e.type === 'click') {
			return;
		}
		var coords = e.type === 'ai' ? e.coords : this.coords;
		var x = coords[0], y = coords[1], winner = null;
		var target = e.type === 'ai' ? _this.virtual[x][y] : this;
		if(!_this.board[x][y]) {
			_this.board[x][y] = _this.turn[1];
			target.classList.add(_this.turn[0]);
			if(Math.abs(_this.board[x][0] + _this.board[x][1] +_this.board[x][2]) === 3){
				winner = _this.board[x][0] + _this.board[x][1] +_this.board[x][2] === 3 ? 'lose' : 'win';
			} else if (Math.abs(_this.board[0][y] + _this.board[1][y] +_this.board[2][y]) === 3) {
				winner = _this.board[0][y] + _this.board[1][y] +_this.board[2][y] === 3 ? 'lose' : 'win';
			} else if (Math.abs(_this.board[0][0] + _this.board[1][1] +_this.board[2][2]) === 3) {
				winner = _this.board[0][0] + _this.board[1][1] +_this.board[2][2] === 3 ? 'lose' : 'win';
			} else if (Math.abs(_this.board[0][2] + _this.board[1][1] +_this.board[2][0]) === 3) {
				winner = _this.board[0][2] + _this.board[1][1] +_this.board[2][0] === 3 ? 'lose' : 'win';
			} 
			if(winner) {
				setTimeout(function(){
					_this.menu.firstElementChild.innerHTML = 'Play Again';
					_this.menu.classList.add(winner);
					_this.menu.style.display = 'block';
				},500);
			} else {
				_this.step++;
				if(_this.step >= 9) {
					_this.menu.firstElementChild.innerHTML = 'Play Again';
					_this.menu.classList.add('tie');
					_this.menu.style.display = 'block';
				} else {
					_this.turn = _this.turn === _this.player ? _this.comp : _this.player;
					if(_this.turn === _this.comp) {
						setTimeout(function(){
							_this.ai(true);
						},500);
					}
				}
			}
		}
	} 

	app.fn.ai = function (test) {
		var x = Math.floor(Math.random() * Math.floor(3));
		var y = Math.floor(Math.random() * Math.floor(3));
		if(test) {
			var z = 0;
			var c = [[],[],[]];
			var d = [[],[]];
			for(i = 0; i < _this.board.length; i++){
				z = _this.board[i].reduce(function(acc,a){return acc + a;},0);
				if(z === -2 && _this.board[i].indexOf(0) >= 0) {
					x = i;
					y = _this.board[i].indexOf(0);
					break;
				} else {
					c[0].push(_this.board[i][0]);
					c[1].push(_this.board[i][1]);
					c[2].push(_this.board[i][2]);
				} 
			}
			for(i = 0; i < c.length; i++){
				z = c[i].reduce(function(acc,a) {return acc + a;},0);
				if(z === -2 && c[i].indexOf(0) >= 0) {
					x = c[i].indexOf(0);
					y = i;
					break;
				} else {
					if(i === 0){
						d[0].push(c[i][0]);
						d[1].push(c[i][2]);
					} else if (i === 1) {
						d[0].push(c[i][1]);
						d[1].push(c[i][1]);
					} else {
						d[0].push(c[i][2]);
						d[1].push(c[i][0]);								
					}
				}
			}
			for(i = 0; i < d.length; i++) {
				z = d[i].reduce(function(acc,a){return acc + a;},0);
				if(z === -2 && d[i].indexOf(0) >= 0) {
					if(i === 0) {
						x = d[i].indexOf(0);
						y = d[i].indexOf(0);
					} else {
						if(d[i].indexOf(0) === 2) {
							x = 0;
							y = 2;
						} else if (d[i].indexOf(0) === 1){
							x = 1;
							y = 1;
						} else {
							x = 2;
							y = 0;
						}
					}
					break;
				}						
			}
		}
		if(!_this.board[x][y]) {
			_this.move({
				type: 'ai',
				coords: [x,y]
			});
		} else {
			_this.ai();
		}
	}

	app.fn.init.prototype = app.fn;

	window['app'] = new app();

}();